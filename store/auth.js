import Cookie from 'js-cookie'

const state = () => ({
	isAuth: false
})

const getters = {
	isAuth: (state) => state.isAuth
}

const actions = {
	login({ commit }, credentials) {
		if (credentials) {
			Cookie.set('jwt', '123456789', { expires: 7 })
			this.$router.push('/')
			commit('setIsAuth', true)
		} else {
			console.log('nothing')
		}
	},
	checkAuth({ commit }) {
		if (Cookie.get('jwt')) {
			commit('setIsAuth', true)
		} else {
			commit('setIsAuth', false)
		}
	},
	logout({ commit }) {
		Cookie.remove('jwt')
		commit('logout')
		this.$router.push('/login')
	}
}

const mutations = {
	setIsAuth(state, isAuth) {
		state.isAuth = isAuth
	},
	logout(state) {
		state.isAuth = false
		state.userInfo = null
	}
}

export default {
	state,
	actions,
	mutations,
	getters
}
