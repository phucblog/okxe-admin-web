import colors from 'vuetify/es5/util/colors'
import { I18N } from './i18n'

const devMod = process.env.NODE_ENV !== 'production'
const proMod = process.env.NODE_ENV === 'production'

export default {
	isDev: devMod,
	isPro: proMod,
	mode: 'universal',
	/*
	** Headers of the page
	*/
	head: {
		titleTemplate: '%s - ' + process.env.npm_package_name,
		title: process.env.npm_package_name || '',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		]
	},
	/*
	** Customize the progress-bar color
	*/
	loading: { color: '#fff' },
	/*
	** Global CSS
	*/
	css: [
		'~/assets/main.sass'
	],
	/*
	** Plugins to load before mounting the App
	*/
	plugins: [
		'~/plugins/axios'
	],
	/*
	** Nuxt.js dev-modules
	*/
	buildModules: [
		// Doc: https://github.com/nuxt-community/eslint-module
		'@nuxtjs/eslint-module',
		'@nuxtjs/vuetify',
		'@nuxtjs/moment'
	],
	/*
	** Nuxt.js modules
	*/
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
		// Doc: https://github.com/nuxt-community/dotenv-module
		'@nuxtjs/dotenv',
		['nuxt-i18n', I18N]
	],

	/*
	** Axios module configuration
	** See https://axios.nuxtjs.org/options
	*/
	axios: {
		baseURL: proMod ? 'helloWorld' : 'http://localhost:8000',
		https: proMod
	},
	/*
	** vuetify module configuration
	** https://github.com/nuxt-community/vuetify-module
	*/
	vuetify: {
		customVariables: ['~/assets/variables.sass'],
		theme: {
			light: true,
			themes: {
				light: {
					primary: '#00BCC3',
					secondary: colors.grey.darken1,
					accent: colors.shades.black,
					info: colors.teal.lighten1,
					warning: colors.amber.base,
					success: colors.green.accent3,
					error: colors.red.accent3
				},
				dark: {
					primary: colors.blue.darken2,
					secondary: colors.amber.darken3,
					accent: colors.grey.darken3,
					info: colors.teal.lighten1,
					warning: colors.amber.base,
					error: colors.deepOrange.accent4,
					success: colors.green.accent3
				}
			}
		}
	},
	/*
	** Build configuration
	*/
	build: {
		/*
		** You can extend webpack config here
		*/
		extend(config, ctx) {
		}
	}
}
