module.exports = {
	root: true,
	env: {
		browser: true,
		node: true
	},
	parserOptions: {
		parser: 'babel-eslint'
	},
	extends: [
		'@nuxtjs',
		'plugin:nuxt/recommended'
	],
	// add your custom rules here
	rules: {
		'vue/html-indent': 'off',
		'no-tabs': 'off',
		'indent': 'off',
		'space-before-function-paren': 'off',
		'arrow-parens': 'off',
		'no-console': 'off',
		'vue/no-unused-components': 'warn'
	}
}
