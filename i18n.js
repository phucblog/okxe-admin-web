import kr from './locales/kr'
import vi from './locales/vi'

export const I18N = {
	locales: [
		{
			code: 'kr',
			iso: 'ko-KO',
			name: 'Korean'
		},
		{
			code: 'vi',
			iso: 'vi-VI',
			name: 'Vietnamese'
		}
	],
	defaultLocale: 'kr',
	vueI18n: {
		fallbackLocale: 'kr',
		messages: { kr, vi }
	}
}
